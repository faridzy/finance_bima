<nav class="pcoded-navbar">
                        <div class="pcoded-inner-navbar main-menu" >
                            <div class="pcoded-navigatio-lavel">Navigation</div>
                            <ul class="pcoded-item pcoded-left-item">
                                <li class="pcoded-trigger">
                                    <a href="{{url('/backend/dashboard')}}">
                                        <span class="pcoded-micon"><i class="feather icon-home"></i></span>
                                            <span class="pcoded-mtext">Dashboard</span>
                                    </a>
                                </li>
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-layers"></i></span>
                                        <span class="pcoded-mtext" >Master</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="{{url('/backend/master/kategori')}}">
                                                <span class="pcoded-mtext" >Kategori Inputan</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="{{url('/backend/master/subkategori')}}">
                                                <span class="pcoded-mtext" >Sub Kategori Inputan</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="{{url('/backend/master/pegawai')}}">
                                                <span class="pcoded-mtext" >Pegawai</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="{{url('/backend/master/rule')}}">
                                                <span class="pcoded-mtext" >Aturan</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="{{url('/backend/master/ratio')}}">
                                                <span class="pcoded-mtext" >Daftar Rasio</span>
                                            </a>
                                        </li>

                                    </ul>
                                </li>
                            </ul>
                            </ul>
                           
                            <div class="pcoded-navigatio-lavel">Support</div>
                            <ul class="pcoded-item pcoded-left-item">
                                <li class="">
                                    <a href="http://html.codedthemes.com/Adminty/doc" target="_blank">
                                        <span class="pcoded-micon"><i class="feather icon-monitor"></i></span>
                                        <span class="pcoded-mtext" >Documentation</span>
                                    </a>
                                </li>
                                <li class="">
                                    <a href="index.html#" target="_blank">
                                        <span class="pcoded-micon"><i class="feather icon-help-circle"></i></span>
                                        <span class="pcoded-mtext" >Submit Issue</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </nav>