@extends('layout.main')
@section('title', $title)
@section('content') 
<div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4>{{$title}}</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{url('/')}}"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Administrator</a> </li>
                        <li class="breadcrumb-item"><a href="#">Manajemen Pengguna</a> </li>
                        <li class="breadcrumb-item"><a href="{{url('/backend/master/rule/')}}">{{$title}}</a> </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                       <h5>{{$title}}</h5>
                        <div class="card-header-right">
                            <ul class="list-unstyled card-option">
                                <li><i class="feather icon-maximize full-card"></i></li>
                                <li><i class="feather icon-minus minimize-card"></i></li>
                                <li><i class="feather icon-trash-2 close-card"></i></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-block">
                        <div class="dt-responsive table-responsive">
                        <p><a onclick="loadModal(this)" target="/backend/master/kategori/form" class="btn btn-primary" style="color: white" title="Tambah Data"><i class="fa fa-plus"></i>Tambah Data</a></p>
                        <div class="dt-responsive table-responsive">
                            <table id="add-row-table" class="table  table-striped table-bordered nowrap">
                                <thead>
                                    <tr>
                                        <th>No</th>                          
                                        <th>Nama</th>                            
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $num => $item)
                                <tr>
                                    <td>{{$num+1}}</td>
                                    <td>{{$item->nama}}</td>
                                    <td>
                                        <a onclick="loadModal(this)" target="/backend/master/kategori/form" data="id={{$item->id}}"
                                           class="btn btn-primary btn-xs icofont icofont-edit" title="Ubah Data"></a>


                                        <a onclick="deleteData({{$item->id}})" class="btn btn-danger btn-xs icofont icofont-trash"
                                           title="Hapus Data"></a>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
@section('scripts')
    <script>
        function deleteData(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Konfirmasi", "Apakah anda yakin akan menghapus data?", function () {
                ajaxTransfer("/backend/master/kategori/delete", data, "#modal-output");
            })
        }
    </script>
    
@endsection    
@endsection