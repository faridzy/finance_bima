<div id="result-form-konten"></div>
<div class="card-block">
<form onsubmit="return false;" id="form-konten" class='form-horizontal'>

    
    <div class='form-group'>
        <label for='nama' class='col-sm-2 col-xs-12 control-label'>Kategori</label>
        <div class='col-sm-9 col-xs-12'>
            <div class="input-group">
            <span class="input-group-addon"><i class="icofont icofont-list"></i></span>
            <input type="text" name="nama" class="form-control" value="{{$data->nama}}" required="">
            </div>
        </div>
    </div> 
    
   
    <div class='form-group'>
        <label class='col-sm-2 col-xs-12 control-label'></label>
        <div class='col-sm-9 col-xs-12'>
            <input type="submit" class="btn btn-primary" value="Simpan Data">
        </div>
    </div>

    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>
</div>
<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/backend/master/kategori/save', data, '#result-form-konten');
        })

        $("#dropper-default").dateDropper( {
        dropWidth: 200,
        dropPrimaryColor: "#1abc9c", 
        dropBorder: "1px solid #1abc9c"
    })
    })
</script>
