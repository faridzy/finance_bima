<div id="result-form-konten"></div>
<div class="card-block">
<form onsubmit="return false;" id="form-konten" class='form-horizontal'>

    <div class='form-group'>
        <label for='nama_ratio' class='col-sm-2 col-xs-12 control-label'>Nama Ratio</label> 
        <div class='col-sm-9 col-xs-12'>
            <div class="input-group">
            <span class="input-group-addon"><i class="icofont icofont-bag-alt"></i></span>
            <input type="text" name="nama_ratio" class="form-control" value="{{$data->nama_ratio}}" required="">
            </div>
        </div>
    </div> 


    <div class='form-group'>
        <label class='col-sm-2 col-xs-12 control-label'></label>
        <div class='col-sm-9 col-xs-12'>
            <input type="submit" class="btn btn-primary" value="Simpan Data">
        </div>
    </div>

    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>
</div>
<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/backend/master/ratio/save', data, '#result-form-konten');
        })
    })
</script>
