<div id="result-form-konten"></div>
<div class="card-block">
<form onsubmit="return false;" id="form-konten" class='form-horizontal'>

    <div class='form-group'>
        <label for='nama' class='col-sm-2 col-xs-12 control-label'>Kategori</label>
        <div class='col-sm-9 col-xs-12'>
            <div class="input-group">
            <span class="input-group-addon"><i class="icofont icofont-list"></i></span>
            <select name="kategori_id" class="form-control">
                @foreach($kategori as $num => $item)
                <option value="{{$item->id}}" @if(!is_null($data)) @if($item->id == $data->kategori_id) selected="selected" @endif @endif >{{$item->nama}}
                </option>
                @endforeach
            </select>
            </div>
        </div>
    </div> 

    <div class='form-group'>
        <label for='nama' class='col-sm-2 col-xs-12 control-label'>Parent Id </label>
        <div class='col-sm-9 col-xs-12'>
            <div class="input-group">
            <span class="input-group-addon"><i class="icofont icofont-list"></i></span>
            <select name="parent_id" class="form-control">
                    <option value="0">Jadikan Induk</option>
                    @foreach($subKategori as $num => $item)
                    @if(isset($data->parent_id))
                        @if($data->parent_id == $item->id)
                            <option value="{{$item->id}}" selected="selected">{{$item->nama}}</option>
                        @else
                            <option value="{{$item->id}}">{{$item->nama}}</option>
                        @endif
                    @else
                        <option value="{{$item->id}}">{{$item->nama}}</option>
                    @endif
                @endforeach
            </select>
            
            </div>
        </div>
    </div>

    <div class='form-group'>
        <label for='nama' class='col-sm-2 col-xs-12 control-label'>Subkategori</label>
        <div class='col-sm-9 col-xs-12'>
            <div class="input-group">
            <span class="input-group-addon"><i class="icofont icofont-list"></i></span>
            <input type="text" name="nama" class="form-control" value="{{$data->nama}}" required="">
            </div>
        </div>
    </div> 
    
    
    
   
    <div class='form-group'>
        <label class='col-sm-2 col-xs-12 control-label'></label>
        <div class='col-sm-9 col-xs-12'>
            <input type="submit" class="btn btn-primary" value="Simpan Data">
        </div>
    </div>

    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>
</div>
<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/backend/master/subkategori/save', data, '#result-form-konten');
        })

        $("#dropper-default").dateDropper( {
        dropWidth: 200,
        dropPrimaryColor: "#1abc9c", 
        dropBorder: "1px solid #1abc9c"
    })
    })
</script>
