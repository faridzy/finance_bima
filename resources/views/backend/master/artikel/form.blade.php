<div id="result-form-konten"></div>
<form onsubmit="return false;" id="form-konten" class='form-horizontal'>
    <div class='form-group'>
        <label for='tittle' class='col-sm-2 col-xs-12 control-label'>Tittle</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="tittle" class="form-control" value="{{$data->tittle}}" required="">
        </div>
    </div>
 
    <div class='form-group'>
        <label for='text_header' class='col-sm-2 col-xs-12 control-label'>Text Header</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="text_header" class="form-control" value="{{$data->text_header}}" required="">
        </div>
    </div>
 
    <div class='form-group'>
        <label for='text' class='col-sm-2 col-xs-12 control-label'>Text</label>
        <div class='col-sm-9 col-xs-12'>
            <textarea type="text" name="text" class="form-control" value="{{$data->text}}" required=""></textarea>
        </div>
    </div>
 
    <div class="form-group">
        <label for='images' class='col-sm-2 col-xs-12 control-label'>Photo</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="file" id="inputImage" name="images" class="validate"/>
        </div>
        <div class='col-sm-9 col-xs-12'>
            @if($data->image=='')
              <img src="{{asset('public/uploads')}}/{{'nopic.png'}}" id="showImage" style="max-width:330px;max-height:270px;float:left;" />
            @else
                <img src="{{asset('public/uploads/artikel')}}/{{$data->images}}" id="showImage" style="max-width:200px;max-height:200px;float:left;" />
            @endif
        </div>
    </div>
 
    <div class='form-group'>
        <label class='col-sm-2 col-xs-12 control-label'></label>
        <div class='col-sm-9 col-xs-12'>
            <input type="submit" class="btn btn-primary" value="Simpan Data">
        </div>
    </div>
 
    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>
 
<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/backend/master/artikel/save', data, '#result-form-konten');
        })
    })
</script>
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
 
            reader.onload = function (e) {
                $('#showImage').attr('src', e.target.result);
            }
 
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#inputImage").change(function () {
        readURL(this);
    });
</script>