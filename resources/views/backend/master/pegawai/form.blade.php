<div id="result-form-konten"></div>
<div class="card-block">
<form onsubmit="return false;" id="form-konten" class='form-horizontal'>

    <div class='form-group'>
        <label for='code_agent' class='col-sm-2 col-xs-12 control-label'>Code Agent</label> 
        <div class='col-sm-9 col-xs-12'>
            <div class="input-group">
            <span class="input-group-addon"><i class="icofont icofont-bag-alt"></i></span>
            <input type="text" name="code_agent" class="form-control" value="{{$data->code_agent}}" required="">
            </div>
        </div>
    </div> 
    <div class='form-group'>
        <label for='nama' class='col-sm-2 col-xs-12 control-label'>Nama</label>
        <div class='col-sm-9 col-xs-12'>
            <div class="input-group">
            <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
            <input type="text" name="nama" class="form-control" value="{{$data->nama}}" required="">
            </div>
        </div>
    </div> 
    <div class='form-group'>
        <label for='email' class='col-sm-2 col-xs-12 control-label'>Email</label>
        <div class='col-sm-9 col-xs-12'>
            <div class="input-group">
            <span class="input-group-addon"><i class="icofont icofont-ui-email"></i></span>
            <input type="email" name="email" class="form-control" value="{{$data->email}}" required="">
            </div>
        </div>
    </div> 
    <div class='form-group'> 
        <label for='password' class='col-sm-2 col-xs-12 control-label'>Password</label>
        <div class='col-sm-9 col-xs-12'>
            <div class="input-group">
            <span class="input-group-addon"><i class="icofont icofont-ui-lock"></i></span>
            <input type="password" name="password" class="form-control">
            </div>
        </div>
    </div>
    <div class='form-group'>
        <label for='tgl_lahir' class='col-sm-2 col-xs-12 control-label'>Tanggal Lahir</label>
            <div class='col-sm-9 col-xs-12'>
                <div class="input-group">
                <span class="input-group-addon"><i class="icofont icofont-ui-calendar"></i></span>
                <input type="date" name="tgl_lahir" class="form-control" value="{{$data->tgl_lahir}}" required=""/>
                </div>
            </div>
    </div>
    <div class='form-group'>
        <label for='user_type' class='col-sm-2 col-xs-12 control-label'>User Type</label>
            <div class='col-sm-9 col-xs-12'>
                <div class="input-group">
                <span class="input-group-addon"><i class="icofont icofont-ui-calendar"></i></span>
                <input type="text" name="user_type" class="form-control" value="{{$data->user_type}}" required="">
                </div>
            </div>
    </div>


    <div class='form-group'>
        <label class='col-sm-2 col-xs-12 control-label'></label>
        <div class='col-sm-9 col-xs-12'>
            <input type="submit" class="btn btn-primary" value="Simpan Data">
        </div>
    </div>

    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>
</div>
<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/backend/master/pegawai/save', data, '#result-form-konten');
        })

        $("#dropper-default").dateDropper( {
        dropWidth: 200,
        dropPrimaryColor: "#1abc9c", 
        dropBorder: "1px solid #1abc9c"
    })
    })
</script>
