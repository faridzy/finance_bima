   <script type="text/javascript" src="{{asset('public/assets/')}}/bower_components/jquery/js/jquery.min.js"></script>
    <script type="text/javascript" src="{{asset('public/assets/')}}/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="{{asset('public/assets/')}}/bower_components/popper.js/js/popper.min.js"></script>
    <script type="text/javascript" src="{{asset('public/assets/')}}/bower_components/bootstrap/js/bootstrap.min.js"></script>
    <!-- Bootstrap date-time-picker js -->
    <script type="text/javascript" src="{{asset('public/assets/template/')}}/pages/advance-elements/moment-with-locales.min.js"></script>
    <script type="text/javascript" src=".{{asset('public/assets/')}}/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="{{asset('public/assets/template')}}/pages/advance-elements/bootstrap-datetimepicker.min.js"></script>
    <!-- Date-range picker js -->
    <script type="text/javascript" src="{{asset('public/assets/')}}/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
    <!-- Date-dropper js -->
    <script type="text/javascript" src="{{asset('public/assets/bower_components/')}}/datedropper/js/datedropper.min.js"></script>
    <!-- Color picker js -->
    <script type="text/javascript" src="{{asset('public/assets/')}}/bower_components/spectrum/js/spectrum.js"></script>
    <script type="text/javascript" src="{{asset('public/assets/')}}/bower_components/jscolor/js/jscolor.js"></script>
    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="{{asset('public/assets/')}}/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
    <!-- modernizr js -->
    <script type="text/javascript" src="{{asset('public/assets/')}}/bower_components/modernizr/js/modernizr.js"></script>
    <!-- Chart js -->
    <script type="text/javascript" src="{{asset('public/assets/')}}/bower_components/chart.js/js/Chart.js"></script>
    <!-- amchart js -->
    <script src="{{asset('public/assets/')}}/template/pages/widget/amchart/amcharts.js"></script>
    <script src="{{asset('public/assets/')}}/template/pages/widget/amchart/serial.js"></script>
    <script src="{{asset('public/assets/')}}/template/pages/widget/amchart/light.js"></script>
    <script src="{{asset('public/assets/')}}/template/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="{{asset('public/assets/')}}/template/js/SmoothScroll.js"></script>
    <script src="{{asset('public/assets/')}}/template/js/pcoded.min.js"></script>
    <script src="{{asset('public/assets/')}}/core/core.js"></script>
    <!-- custom js -->
    <script src="{{asset('public/assets/')}}/template/js/vartical-layout.min.js"></script>
    <script type="text/javascript" src="{{asset('public/assets/')}}/template/pages/dashboard/custom-dashboard.js"></script>
    <script type="text/javascript" src="{{asset('public/assets/')}}/template/js/script.min.js"></script>
    <script type="text/javascript" src="{{asset('public/assets/')}}/bower_components/datedropper/js/datedropper.min.js"></script>
    <script type="text/javascript" src="{{asset('public/assets/template/')}}/pages/advance-elements/custom-picker.js"></script>
     <!-- data-table js -->
    <script src="{{asset('public/assets/')}}/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('public/assets/')}}/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="{{asset('public/assets/template/')}}/pages/data-table/js/jszip.min.js"></script>
    <script src="{{asset('public/assets/template/')}}/pages/data-table/js/pdfmake.min.js"></script>
    <script src="{{asset('public/assets/template/')}}/pages/data-table/js/vfs_fonts.js"></script>
    <script src="{{asset('public/assets/')}}/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="{{asset('public/assets/')}}/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="{{asset('public/assets/')}}/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{asset('public/assets/')}}/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="{{asset('public/assets/')}}/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<input type='hidden' name='_token' value='{{ csrf_token() }}'>
<script>
    baseURL = '{{url("/")}}';
    window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>
@yield('scripts')