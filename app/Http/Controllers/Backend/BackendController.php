<?php


namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Models\Pegawai;
use App\Models\Client;
use App\Http\Controllers\Controller;

class BackendController extends Controller {

    public function index()
    {
        if (empty(session('activeUser'))) {
            return redirect('login');
        }
        $rolePegawai=2; $roleClient=2; 
        $pegawai=Pegawai::where('user_type', 'LIKE', '%'.$rolePegawai.'%')->count();
        $client=Client::count();
        //$lastUpdateClient=Client::orderBy('created_at', 'DESC')->first();
        //$lastUpdateClient=$lastUpdateClient->created_at;
        //$lastUpdatePegawai=Pegawai::orderBy('created_at', 'DESC')->first();
        //$lastUpdatePegawai=$lastUpdateClient->created_at;
        //dd($lastUpdate);
        $pegawai=Pegawai::where('user_type', 'LIKE', '%'.$rolePegawai.'%')->count();
        $params=[
            'pegawai'=>$pegawai,
            'client'=>$client,
            'title'=>'Manajemen Dashboard'
        ];
        return view('backend.dashboard.index', $params);
    }
}