<?php
 
namespace App\Http\Controllers\Backend\Master;


use App\Http\Controllers\Controller;
use App\Models\SubKategori;
use App\Models\Kategori;
use Illuminate\Http\Request;

class SubKategoriController extends Controller
{
    public function index(){
        $data=SubKategori::all();
        $params=[
            'data'=>$data,
            'title'=>'Manajemen SubKategori'
        ];

        return view('backend.master.subkategori.index',$params);
    }

    public function form(Request $request){
        $id = $request->input('id');
        $kategori=Kategori::all();
        $subKategori=SubKategori::all();
        if($id){
            $data = SubKategori::find($id);
        }else{
            $data = new SubKategori();
        }
        $params = [
            'title' => 'Manajemen SubKategori',
            'data' => $data,
            'kategori' => $kategori,
            'subKategori' => $subKategori
        ];
        return view('backend.master.subkategori.form',$params);
    }

    public  function  save(Request $request){
        $id = intval($request->input('id', 0));
        if($id){
            $data = SubKategori::find($id);
        }else{
            $data = new SubKategori();
            $cek=SubKategori::where(['nama' => $request->nama])->first();
            if(!is_null($cek)){
                return "<div class='alert alert-danger'>Terjadi kesalahan! SubKategori sudah tersedia!</div>";
            }

        }
        $data->parent_id=$request->parent_id;
        $data->nama = $request->nama;
        $data->kategori_id = $request->kategori_id;
        
        
        try{
            $data->save();
            return "
            <div class='alert alert-success'>SubKategori berhasil disimpan!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            dd($ex);
            return "<div class='alert alert-danger'>Terjadi kesalahan! SubKategori gagal disimpan!</div>";
        }

    }

    public function delete(Request $request){
        $id = intval($request->input('id', 0));
        try{
            SubKategori::find($id)->delete();
            return "
            <div class='alert alert-success'>SubKategori berhasil dihapus!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! SubKategori gagal dihapus!</div>";
        }

    }
}