<?php

namespace App\Http\Controllers\Backend\Master;


use App\Http\Controllers\Controller;
use App\Models\Pegawai;
use Illuminate\Http\Request;

class PegawaiController extends Controller
{
    public function index(){
        $rolePegawai=2; $roleClient=2; 
        $pegawai=Pegawai::where('user_type', 'LIKE', '%'.$rolePegawai.'%')->count();
        //$pegawai=Pegawai::where('user_type', 'LIKE', '%'.$rolePegawai.'%')->count();
        $params=[
            'pegawai'=>$pegawai,
            'title'=>'Manajemen Dashboard'
        ];

        return view('backend.dashboard.index',$params);
    }
}