<?php

namespace App\Http\Controllers\Backend\Master;


use App\Http\Controllers\Controller;
use App\Models\Ratio;
use Illuminate\Http\Request;

class RatioController extends Controller
{
    public function index(){
        $data=Ratio::all();
        $params=[
            'data'=>$data,
            'title'=>'Manajemen Ratio'
        ];

        return view('backend.master.ratio.index',$params);
    }

    public function form(Request $request){
        $id = $request->input('id');
        if($id){
            $data = Ratio::find($id);
        }else{
            $data = new Ratio();
        }
        $params = [
            'title' => 'Manajemen Ratio',
            'data' => $data
        ];
        return view('backend.master.ratio.form',$params);
    }

    public  function  save(Request $request){
        $id = intval($request->input('id', 0));
        if($id){
            $data = Ratio::find($id);
        }else{
            $data = new Ratio();
            $cek=Ratio::where(['nama_ratio' => $request->nama_ratio])->first();
            if(!is_null($cek)){
                return "<div class='alert alert-danger'>Terjadi kesalahan! Ratio sudah tersedia!</div>";
            }

        }
        $data->nama_ratio = $request->nama_ratio;
               
        try{
            $data->save();
            return "
            <div class='alert alert-success'>Ratio berhasil disimpan!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Ratio gagal disimpan!</div>";
        }

    }

    public function delete(Request $request){
        $id = intval($request->input('id', 0));
        try{
            Ratio::find($id)->delete();
            return "
            <div class='alert alert-success'>Ratio berhasil dihapus!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Pegawai gagal dihapus!</div>";
        }

    }
}