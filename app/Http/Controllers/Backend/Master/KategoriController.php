<?php

namespace App\Http\Controllers\Backend\Master;


use App\Http\Controllers\Controller;
use App\Models\Kategori;
use Illuminate\Http\Request;

class KategoriController extends Controller
{
    public function index(){
        $data=Kategori::all();
        $params=[
            'data'=>$data,
            'title'=>'Manajemen Kategori'
        ];

        return view('backend.master.kategori.index',$params);
    }

    public function form(Request $request){
        $id = $request->input('id');
        if($id){
            $data = Kategori::find($id);
        }else{
            $data = new Kategori();
        }
        $params = [
            'title' => 'Manajemen Kategori',
            'data' => $data,
            'kategori' => Kategori::all()
        ];
        return view('backend.master.kategori.form',$params);
    }

    public  function  save(Request $request){
        $id = intval($request->input('id', 0));
        if($id){
            $data = Kategori::find($id);
        }else{
            $data = new Kategori();
            $cek=Kategori::where(['nama' => $request->nama])->first();
            if(!is_null($cek)){
                return "<div class='alert alert-danger'>Terjadi kesalahan! Kategori sudah tersedia!</div>";
            }

        }
        $data->nama = $request->nama;
        
        try{
            $data->save();
            return "
            <div class='alert alert-success'>Kategori berhasil disimpan!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Kategori gagal disimpan!</div>";
        }

    }

    public function delete(Request $request){
        $id = intval($request->input('id', 0));
        try{
            Kategori::find($id)->delete();
            return "
            <div class='alert alert-success'>Kategori berhasil dihapus!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Kategori gagal dihapus!</div>";
        }

    }
}