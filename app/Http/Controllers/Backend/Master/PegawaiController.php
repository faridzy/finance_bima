<?php

namespace App\Http\Controllers\Backend\Master;


use App\Http\Controllers\Controller;
use App\Models\Pegawai;
use Illuminate\Http\Request;

class PegawaiController extends Controller
{
    public function index(){
        $data=Pegawai::all();
        $params=[
            'data'=>$data,
            'title'=>'Manajemen Pegawai'
        ];

        return view('backend.master.pegawai.index',$params);
    }

    public function form(Request $request){
        $id = $request->input('id');
        if($id){
            $data = Pegawai::find($id);
        }else{
            $data = new Pegawai();
        }
        $params = [
            'title' => 'Manajemen Pegawai',
            'data' => $data
        ];
        return view('backend.master.pegawai.form',$params);
    }

    public  function  save(Request $request){
        $id = intval($request->input('id', 0));
        if($id){
            $data = Pegawai::find($id);
        }else{
            $data = new Pegawai();
            $cek=Pegawai::where(['code_agent' => $request->code_agent])->first();
            if(!is_null($cek)){
                return "<div class='alert alert-danger'>Terjadi kesalahan! Code Agent sudah tersedia!</div>";
            }

        }
        $data->code_agent = $request->code_agent;
        $data->nama = $request->nama;
        $data->email = $request->email; 
        if(!is_null(str_replace(' ','',$request->password))){
            $password = sha1($request->password);
        }else{
            $password=$data->password;
        }
        $data->password=$password;
        $data->tgl_lahir = $request->tgl_lahir;
        $data->user_type = $request->user_type;
               
        try{
            $data->save();
            return "
            <div class='alert alert-success'>Pegawai berhasil disimpan!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Pegawai gagal disimpan!</div>";
        }

    }

    public function delete(Request $request){
        $id = intval($request->input('id', 0));
        try{
            Pegawai::find($id)->delete();
            return "
            <div class='alert alert-success'>Pegawai berhasil dihapus!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Pegawai gagal dihapus!</div>";
        }

    }
}