<?php

namespace App\Http\Controllers\Backend\Master;


use App\Http\Controllers\Controller;
use App\Models\Artikel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

class ArtikelController extends Controller
{
    public function index(){
        $data=Artikel::all();
        $params=[
            'data'=>$data,
            'title'=>'Manajemen Artikel'
        ];

        return view('backend.master.artikel.index',$params);
    }

    public function form(Request $request){
        $id = $request->input('id');
        if($id){
            $data = Artikel::find($id);
        }else{
            $data = new Artikel();
        }
        $params = [
            'title' => 'Manajemen Artikel',
            'data' => $data
        ];
        return view('backend.master.artikel.form',$params);
    }

    public  function  save(Request $request){
        $id = intval($request->input('id', 0));
        if($id){
            $data = Artikel::find($id);
        }else{
            $data = new Artikel();
            $cek=Artikel::where(['tittle' => $request->tittle])->first();
            if(!is_null($cek)){
                return "<div class='alert alert-danger'>Terjadi kesalahan! title sudah tersedia!</div>";
            }

        }
        $data->tittle = $request->tittle;
        $data->text_header = $request->text_header;
        $data->text = $request->text;
        
        if(!is_null($data->images)){
            if(is_null($request->file('images')) && $request->file('images')==''){
                $fileImage=$data->images;
            }else{
                $rules=[
                    'images' => 'mimes:jpeg,bmp,png,gif,jpg,pdf',
                ];
                $validator=Validator::make($request->all(),$rules);
                if($validator->fails()){
                    return "<div class='alert alert-danger'>Terjadi kesalahan!Format tidak valid!</div>";
                }else{
                    $fotoImage=$request->file('images');
                    $destinationPath = 'public/upload/Artikel/'.$request->image_id;
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $fileImage = date('YmdHis') . '_' . $fotoImage->getClientOriginalName();
                    $fotoImage->move($destinationPath, $fileImage);
                }
 
            }
        }
 
        if(is_null($data->images)){
            if(is_null($request->file('images')) && $request->file('images')==''){
                $fileImage=$data->images;
            }else{
                $rules=[
                    'images' => 'mimes:jpeg,bmp,png,gif,jpg,pdf',
                ];
                $validator=Validator::make($request->all(),$rules);
                if($validator->fails()){
                    return "<div class='alert alert-danger'>Terjadi kesalahan!Format tidak valid!</div>";
                }else{
                    $fotoImage=$request->file('images');
                    $destinationPath = 'public/upload/Artikel/'.$request->image_id;
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $fileImage = date('YmdHis') . '_' . $fotoImage->getClientOriginalName();
                    $fotoImage->move($destinationPath, $fileImage);
                }
 
            }
        }
        $data->images = $fileImage;
        
               
        try{
            $data->save();
            return "
            <div class='alert alert-success'>Artikel berhasil disimpan!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Artikel gagal disimpan!</div>";
        }

    }

    public function delete(Request $request){
        $id = intval($request->input('id', 0));
        try{
            Artikel::find($id)->delete();
            return "
            <div class='alert alert-success'>Ratio berhasil dihapus!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Artikel gagal dihapus!</div>";
        }

    }
}