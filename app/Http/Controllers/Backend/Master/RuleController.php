<?php

namespace App\Http\Controllers\Backend\Master;


use App\Http\Controllers\Controller;
use App\Models\Rule;
use App\Models\Ratio;
use Illuminate\Http\Request;

class RuleController extends Controller
{
    public function index(){
        $data=Rule::all();
        $params=[
            'data'=>$data,
            'title'=>'Manajemen Rule'
        ];

        return view('backend.master.rule.index',$params);
    }

    public function form(Request $request){
        $id = $request->input('id');
        $ratio = Ratio::all();
        if($id){
            $data = Rule::find($id);
        }else{
            $data = new Rule();
        }
        $params = [
            'title' => 'Manajemen Rule',
            'data' => $data,
            'ratio' => $ratio,
        ];
        return view('backend.master.rule.form',$params);
    }

    public  function  save(Request $request){
        $id = intval($request->input('id', 0));
        if($id){
            $data = Rule::find($id);
        }else{
            $data = new Rule();
        }
        $data->range = $request->range;
        $data->nilai = $request->nilai;
        $data->statement = $request->statement;
        $data->saran = $request->saran; 
        $data->ratio_id = $request->ratio_id;
               
        try{
            $data->save();
            return "
            <div class='alert alert-success'>Rule berhasil disimpan!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Rule gagal disimpan!</div>";
        }

    }

    public function delete(Request $request){
        $id = intval($request->input('id', 0));
        try{
            Rule::find($id)->delete();
            return "
            <div class='alert alert-success'>Rule berhasil dihapus!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Rule gagal dihapus!</div>";
        }

    }
}