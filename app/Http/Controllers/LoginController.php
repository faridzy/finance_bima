<?php

namespace App\Http\Controllers;
//use App\Clasess\CaptchaClass;
use App\Models\Pegawai;
//use App\Models\User;
use Illuminate\Http\Request;
use Session;

class LoginController extends Controller
{

    //private $captcha;

    public function __construct()
    {
        //$this->captcha=new CaptchaClass();

    }

    public function index(){
        if (!empty(session('activeUser'))) {
            return redirect('/');
        }
        return view('login.index');
    }

    public  function login(Request $request){
        $code_agent=$request->input('code_agent');
        $password=$request->input('password');
        

        $activeUser=Pegawai::where(['code_agent'=>$code_agent])->first();
        if(is_null($activeUser)){
            return "<div class='alert alert-danger'>Code Agent Tidak Ditemukan!</div>";

        }else{
            if($activeUser->password !=sha1($password)){
                return "<div class='alert alert-danger'>Code Agent atau Password Salah!</div>";
            }
            else{
                $request->session()->put('activeUser', $activeUser);

                return "
            <div class='alert alert-success'>Login berhasil!</div>
            <script> scrollToTop(); reload(1000); </script>";
            }
        }


    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect('/');
    }


}