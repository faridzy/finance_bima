<?php

namespace App\Http\Controllers\Api\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Client;
use App\Models\Pegawai;


class ApiRegisterClientController extends Controller
{   

    public  function save(Request $request){
        $data= new Client();
        $data->nama=$request->nama;
        $data->email=$request->email;
        $data->password=sha1($request->password);        
        $data->pekerjaan=$request->pekerjaan;
        $data->tgl_lahir=$request->tgl_lahir;
        $data->telp=$request->telp;
        $data->alamat=$request->alamat;
        
        

        $cekPegawai=Pegawai::where('code_agent',$request->pegawai_code)->first();
        if(is_null($cekPegawai)){
            return response()->json([
                'message'=>'Code Agent Salah',
                'data'=> null
            ]);
        }
        $data->pegawai_code=$cekPegawai->id;
           
        try{
            $data->save();
            return response()->json([
                'code'=>302,
                'message'=>'Success save client!',
                'data'=>$data
            ]);

        } catch (\Exception $ex){
            return response()->json([
                'message'=>$ex
            ]);
        }

    }
}