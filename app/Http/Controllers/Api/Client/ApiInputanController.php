<?php

namespace App\Http\Controllers\Api\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Client;
use App\Models\Pegawai;
use App\Models\Kategori;
use App\Models\Value;
use App\Models\Rule;
use App\Models\History;
use App\Models\HistoryStatement;



class ApiInputanController extends Controller
{ 


    public function all(){
        $parentId=0;
        $dataKategori=Kategori::with('getSubKategori')->get();
        $currentData=[];
        foreach ($dataKategori as $key => $value) {
            foreach ($value->getSubKategori as $key2 => $item) {
                if($item->getChild->count()!=0){
                    $currentData[]=[
                    'id'=>$value->id,
                    'nama_kategori'=>$value->nama,
                    'sub_kategori'=>$item
                    ];
                }
            }
        }
        return response()->json($currentData);
    } 



    public  function add($id, Request $request){
        $id=$id;
        $cek = Client::where('id',$id)->first();
        $data=json_decode($request->data);

        //dd($data);
    
        try{
                foreach ($data as $key => $value) {
                $newData=New Value();
                $newData->sub_kategori_id=$value->sub_kategori_id;
                $newData->value=$value->value;
                $newData->client_id=$id;
                $newData->save();
            }
        
            return response()->json([
                'id'=>$id,
                'code'=>302,
                'message'=>'Success input!',
                'data'=>$data
            ]);

        } catch (Exception $ex){
            return response()->json([
                'message'=>$ex
            ]);
        }

    }

    public function Hitungan($id){
        $id=$id;
        $cek = Client::where('id',$id)->first();

        $cekValue=Value::where(['client_id'=>$id])->whereDate('created_at', '=', date('Y-m-d'))->get();

        $valueKas=0;
        $valuePengeluaran=0;
        $valueKekayaan=0;
        $valueTabungan=0;
        $valueBruto=0;
        $valueHutang=0;
        $valueAsset=0;
        $valueInvestasi=0;
        $valuePengeluaranTetap=0;
        $valuePribadi=0;
        $valuePengeluaranVariabel=0;
        $valueHutangPendek=0;
        $valueHutangPanjang=0;
        $valuePengeluaranBiasa1=0;
        $valuePengeluaranBiasa2=0;
        $valueGajiTetap=0;
        $valueGajiTTetap=0;
        $valueTabunganBiasa=0;

        foreach ($cekValue as $key => $value) {
            if(!is_null($value->getSubKategori)){
                $data=$value->getSubKategori;
                if($data->id==17){
                    $valueTabunganBiasa=$value->value;
                }
                if($data->id==17 || $data->id==18){
                    $valueKas+=$value->value;
                }
                if($data->id==19 ||$data->id==20 ||$data->id==21 ||$data->id==22){
                    $valuePribadi+=$value->value;    
                }if($data->id==23 ||$data->id==24 ||$data->id==25 ||$data->id==26 ||$data->id==27 ||$data->id==28){
                    $valueInvestasi+=$value->value;    
                }if($data->id==33 ||$data->id==34 ||$data->id==35 ||$data->id==36){
                    $valuePengeluaranTetap+=$value->value;    
                }if($data->id==37 ||$data->id==38 ||$data->id==39 ||$data->id==40){
                    $valuePengeluaranVariabel+=$value->value;    
                }if($data->id==10){
                    $valuePengeluaranBiasa1=$value->value;    
                }if($data->id==11){
                    $valuePengeluaranBiasa2=$value->value;    
                }if($data->id==8){
                    $valueGajiTetap=$value->value;    
                }if($data->id==9){
                    $valueGajiTTetap=$value->value;    
                }if($data->id==29){
                    $valueHutangPendek+=$value->value;    
                }if($data->id==30 ||$data->id==31){
                    $valueHutangPanjang+=$value->value;    
                }
            }
        }
            
        

        $totalPengeluaran=$valuePengeluaranTetap+$valuePengeluaranVariabel+$valuePengeluaranBiasa1+$valuePengeluaranBiasa2;
        $totalKekayaan=$valueKas+$valuePribadi+$valueInvestasi;
        $totalBruto=$valueGajiTetap+$valueGajiTTetap;
        $totalHutang=$valueHutangPendek+$valueHutangPanjang;
        $totalBersihKekayaan=$totalKekayaan-$totalHutang;

        #RASIO
        $rasioLikuid= $valueKas/$totalPengeluaran;
        $rasioLikuidKekayaan= ($valueKas/$totalBersihKekayaan)*100;
        $rasioTabungan= ($valuePengeluaranBiasa1/$totalBruto)*100;
        $rasioHutangAsset= ($totalHutang/$totalKekayaan)*100;
        $rasioPelunasanHutang= ($valuePengeluaranTetap/$totalBruto)*100;
        $rasioAssetInvestKekayaan= ($valueInvestasi/$totalBersihKekayaan)*100;

        for ($i=1; $i <=6 ; $i++) { 
        $dataHistory= new history();
        $dataHistory->client_id=$id;
        $dataHistory->pegawai_id=$cek->pegawai_code;
        if($i==1){
            $dataHistory->value=$rasioLikuid;
            $dataHistory->ratio_id=1;
        }elseif ($i==2) {
            $dataHistory->value=$rasioLikuidKekayaan;
            $dataHistory->ratio_id=2;
        }elseif ($i==3) {
            $dataHistory->value=$rasioTabungan;
            $dataHistory->ratio_id=3;
        }elseif ($i==4) {
            $dataHistory->value=$rasioHutangAsset;
            $dataHistory->ratio_id=4;
        }elseif ($i==5) {
            $dataHistory->value=$rasioPelunasanHutang;
            $dataHistory->ratio_id=5;
        }elseif ($i==6) {
            $dataHistory->value=$rasioAssetInvestKekayaan;
            $dataHistory->ratio_id=6;
        }
            $dataHistory->save();
            $historyStatement= new HistoryStatement();
            $historyStatement->history_id=$dataHistory->id;
            if($i==1){
              $historyStatement->rule_id=$this->getRule($rasioLikuid,1);  
            }elseif ($i==2) {
                $historyStatement->rule_id=$this->getRule($rasioLikuidKekayaan,2);
            }elseif ($i==3) {
                $historyStatement->rule_id=$this->getRule($rasioTabungan,3);
            }elseif ($i==4) {
                $historyStatement->rule_id=$this->getRule($rasioHutangAsset,4);
            }elseif ($i==5) {
                $historyStatement->rule_id=$this->getRule($rasioPelunasanHutang,5);
            }elseif ($i==6) {
                $historyStatement->rule_id=$this->getRule($rasioAssetInvestKekayaan,6);
            }
            $historyStatement->save();
        }

        $dataRule=History::where('client_id',$id)->whereDate('created_at', '=', date('Y-m-d'))->get();
        $curData=[];
        
        foreach ($dataRule as $key => $value) {
            $curData[]=[
                'ratio'=>$value->getRatio->nama_ratio,
                'rule_statement'=>$value->getHistoryStatement->getRule->statement,
                'rule_saran'=>$value->getHistoryStatement->getRule->saran,
            ];
        }
        //dd($curData);
        return response()->json([
                'id'=>$id,
                'code'=>302,
                'message'=>'Success hitung!',
                'data'=>[
                    'rasioLikuid'=>number_format($rasioLikuid, 1),
                    'rasioLikuidKekayaan'=>number_format($rasioLikuidKekayaan, 2),
                    'rasioTabungan'=>number_format($rasioTabungan, 2),
                    'rasioHutangAsset'=>number_format($rasioHutangAsset, 2),
                    'rasioPelunasanHutang'=>number_format($rasioPelunasanHutang, 2),
                    'rasioAssetInvestKekayaan'=>number_format($rasioAssetInvestKekayaan, 2),
                ],
                'dataRule'=>$curData
            ]);
      }

    public function getRule($value,$ratio_id){
        $data=Rule::all();

        foreach ($data as $key => $item) {
            $rule=explode('-', $item->range);
            if($item->ratio_id==$ratio_id){
                 if(($rule[0] <= $value) && ($value <= $rule[1])){
                 return $item->id;
                }   
            } 
        }
    }
}