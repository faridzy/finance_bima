<?php


namespace App\Http\Controllers\Api\Client;
use App\Http\Controllers\Controller;
use App\Models\Artikel;
use App\Models\Client;

class ApiArtikelController extends Controller
{

    public function index(){

        $data = Artikel::all();


        $params = [
            'code' => 302,
            'description' => 'Found',
            'message' => 'Get Artikel Success!',
            'data' => $data
        ];

        return response()->json($params);
    }

    public function detail($id){
        $data = Artikel::where('id',$id)->get();

        $params = [
            'code' => 302,
            'description' => 'Found',
            'message' => 'Get artikel Success!',
            'data' => $data
        ];

        return response()->json($params);
    }


}