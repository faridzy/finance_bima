<?php

namespace App\Http\Controllers\Api\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Client;

class ApiLoginClientController extends Controller
{   

    public function login(Request $request){
        $email=$request->email;
        $password=sha1($request->password);

        if(is_null($email)){
            return response()->json([
                'code'=>404,
                'message'=>'Missing required parameter email!',
                'data'=>null
            ]);
        }

        $activeUser=Client::where(['email'=>$email])->first();
        
        if(is_null($activeUser)){
            return response()->json([
                'code'=>404,
                'message'=>'Email not Found!',
                'data'=>null
            ]);
        }

        if($activeUser->password != $password){
            return response()->json([
                'code'=>404,
                'message'=>'Password not Match!',
                'data'=>null
            ]);
        }

        $data = [
            'id' => $activeUser->id,
            'nama'=>$activeUser->nama,
            'pekerjaan'=>$activeUser->pekerjaan,
            'email'=>$activeUser->email,
            'tgl_lahir'=>$activeUser->tgl_lahir,
            'telp'=>$activeUser->telp,
        ];

        $params = [
            'code' => 302,
            'description' => 'Found',
            'message' => 'Login Success!',
            'data' => $data
        ];


        return response()->json($params);
    }

}