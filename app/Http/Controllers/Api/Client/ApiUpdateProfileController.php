<?php

namespace App\Http\Controllers\Api\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Client;

class ApiUpdateProfileController extends Controller
{
    public function update($id, Request $request){
            $id=$id;
            $nama=$request->nama;
            $pekerjaan=$request->pekerjaan;
            $email=$request->email;
            $tgl_lahir=$request->tgl_lahir;
            $telp=$request->telp;  
            $password=$request->password;           

            $sendingParams = [
                'id'=>$id,
                'nama' => $nama,
                'pekerjaan' => $pekerjaan,
                'email'=>$email,
                'tgl_lahir'=>$tgl_lahir,
                'telp'=>$telp,
                'password'=>$password,
            ];

            if(is_null($nama)){
            return response()->json([
                'code'=>404,
                'message'=>'Missing required parameter nama!',
                'data'=>null
                ]);
            }

            if(is_null($pekerjaan)){
            return response()->json([
                'code'=>404,
                'message'=>'Missing required parameter pekerjaan!',
                'data'=>null
                ]);
            }

            if(is_null($email)){
            return response()->json([
                'code'=>404,
                'message'=>'Missing required parameter email!',
                'data'=>null
                ]);
            }

            if(is_null($tgl_lahir)){
            return response()->json([
                'code'=>404,
                'message'=>'Missing required parameter tgl_lahir!',
                'data'=>null
                ]);
            }

            if(is_null($telp)){
            return response()->json([
                'code'=>404,
                'message'=>'Missing required parameter telp!',
                'data'=>null
                ]);
            }

            $data=Client::find($id);
            if(is_null($data)){
            return response()->json([
                'code'=>404,
                'message'=>'Missing required parameter id!',
                'data'=>null
                ]);
            }

            try{

                $data->nama=$nama;
                $data->pekerjaan=$pekerjaan;
                $data->email=$email;
                $data->tgl_lahir=$tgl_lahir;
                $data->telp=$telp;  
                if(!is_null($password)){
                    $data->password=sha1($password);
                }     
                $data->save();

                $params = [
                    'code' => 302,
                    'description' => 'Found',
                    'message' => 'Update Profile success!',
                    'data' => $data
                ];
                return response()->json($params);

            }catch (\Exception $ex){
            return response()->json([
                'message'=>$ex->getMessage()
                ]);
            }

        }


        public function getUserById($id){
            $data=Client::find($id);
            $params = [
                'code' => 302,
                'description' => 'Found',
                'message' => 'Get Data success!',
                'data' => $data
            ];
            if(!is_null($data)){
                return response()->json($params);
            }
        }
}