<?php


namespace App\Http\Controllers\Api\Pegawai;
use App\Http\Controllers\Controller;
use App\Models\Pegawai;
use App\Models\Client;

class ApiListClientController extends Controller
{

    public function list($pegawai_code){

        $data = Client::where('pegawai_code',$pegawai_code)->orderBy('nama', 'ASC')->get();

        $detailClient=[];
        foreach ($data as $key => $item) {
                $detailClient[]=[
                'id_client'=>$item->id,
                'nama'=>$item->nama,
                'pekerjaan'=>$item->pekerjaan,
                'alamat'=>$item->alamat,
                'tgl_lahir'=>$item->tgl_lahir,
                'email'=>$item->email,
                'telp'=>$item->telp
            ];

        }

        $params = [
            'code' => 302,
            'description' => 'Found',
            'message' => 'Get client Success!',
            'data' => $detailClient
        ];

        return response()->json($params);
    }


}