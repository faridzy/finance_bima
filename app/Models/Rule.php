<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Rule extends Model
{
    protected $table = 'rule';
    protected $primaryKey = 'id';
    public $timestamps = true; 

    public function getRatio()
    {
        return $this->hasOne('App\Models\Ratio','id','ratio_id');
    }
}