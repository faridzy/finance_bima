<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class HistoryStatement extends Model
{
    protected $table = 'history_statement';
    protected $primaryKey = 'id';
    public $timestamps = true; 

    public function getRule()
    {
        return $this->hasOne('App\Models\Rule','id','rule_id');
    }

    public function getHistory()
    {
        return $this->hasOne('App\Models\History','id','history_id');
    }
}