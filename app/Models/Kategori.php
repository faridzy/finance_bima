<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = 'kategori';
    protected $primaryKey = 'id';
    public $timestamps = true; 


    public function getSubKategori()
    {
        return $this->hasMany('App\Models\SubKategori','kategori_id','id');
    }
}