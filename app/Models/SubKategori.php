<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class SubKategori extends Model
{
    protected $table = 'sub_kategori';
    protected $primaryKey = 'id';
    public $timestamps = true; 



    public function getKategori()
    {
        return $this->hasOne('App\Models\Kategori','id','kategori_id');
    }


     public function getParent()
    {
        return $this->hasOne('App\Models\SubKategori','id','parent_id');
    }

     public function getChild()
    {
        return $this->hasMany('App\Models\SubKategori','parent_id','id');
    }      
}