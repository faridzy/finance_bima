<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $table = 'history';
    protected $primaryKey = 'id';
    public $timestamps = true; 

    public function getClient()
    {
        return $this->hasOne('App\Models\Client','id','client_id');
    }
    public function getRatio()
    {
        return $this->hasOne('App\Models\Ratio','id','ratio_id');
    }
    public function getPegawai()
    {
        return $this->hasOne('App\Models\Pegawai','id','pegawai_id');
    }
    public function getValue()
    {
        return $this->hasOne('App\Models\Value','id','value_id');
    }

     public function getHistoryStatement()
    {
        return $this->hasOne('App\Models\HistoryStatement','history_id','id');
    }


}