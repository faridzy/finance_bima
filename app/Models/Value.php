<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Value extends Model
{
    protected $table = 'value';
    protected $primaryKey = 'id';
    public $timestamps = true; 

    public function getSubKategori()
    {
        return $this->hasOne('App\Models\SubKategori','id','sub_kategori_id');
    }
    public function getClient()
    {
        return $this->hasOne('App\Models\Client','id','client_id');
    }
}