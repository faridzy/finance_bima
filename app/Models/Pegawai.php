<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    protected $table = 'pegawai';
    protected $primaryKey = 'id';
    public $timestamps = true;


    public function getClientHistory()
    {
        return $this->hasOne('App\Models\History','pegawai_id','id');
    }
    public function getClient()
    {
        return $this->hasMany('App\Models\Client','pegawai_code','id');
    }

}