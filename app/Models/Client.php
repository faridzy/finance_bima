<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = 'client';
    protected $primaryKey = 'id';
    public $timestamps = true; 

    public function getHistory()
    {
        return $this->hasMany('App\Models\History','client_id','id');
    }

    public function getPegawai()
    {
    	return $this->hasMany('App\Models\Pegawai','id','pegawai_id');
    }
}