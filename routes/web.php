<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Backend\BackendController@index');
Route::get('/login', 'LoginController@index');
Route::get('/logout', 'LoginController@logout');
Route::post('validate-login', 'LoginController@login');
Route::group(['prefix' => 'backend', 'namespace' => 'Backend','middleware' => ['verify-login']], function () {
    Route::get('/dashboard', 'BackendController@index');
    Route::group(['prefix' => 'master', 'namespace' => 'Master'], function () {
        Route::group(['prefix' => 'kategori'], function () {
            Route::get('/', 'KategoriController@index');
            Route::post('/form', 'KategoriController@form');
            Route::post('/save', 'KategoriController@save');
            Route::post('/delete', 'KategoriController@delete');
        });

        Route::group(['prefix' => 'subkategori'], function () {
            Route::get('/', 'SubKategoriController@index');
            Route::post('/form', 'SubKategoriController@form');
            Route::post('/save', 'SubKategoriController@save');
            Route::post('/delete', 'SubKategoriController@delete');
        });

        Route::group(['prefix' => 'pegawai'], function () {
            Route::get('/', 'PegawaiController@index');
            Route::post('/form', 'PegawaiController@form');
            Route::post('/save', 'PegawaiController@save');
            Route::post('/delete', 'PegawaiController@delete');
        });

        Route::group(['prefix' => 'ratio'], function () {
            Route::get('/', 'RatioController@index');
            Route::post('/form', 'RatioController@form');
            Route::post('/save', 'RatioController@save');
            Route::post('/delete', 'RatioController@delete');
        });

        Route::group(['prefix' => 'rule'], function () {
            Route::get('/', 'RuleController@index');
            Route::post('/form', 'RuleController@form');
            Route::post('/save', 'RuleController@save');
            Route::post('/delete', 'RuleController@delete');
        });

        Route::group(['prefix' => 'artikel'], function () {
            Route::get('/', 'ArtikelController@index');
            Route::post('/form', 'ArtikelController@form');
            Route::post('/save', 'ArtikelController@save');
            Route::post('/delete', 'ArtikelController@delete');
        });

        Route::group(['prefix' => 'realisasi'], function () {
            Route::get('/', 'RealisasiController@index');
            Route::get('/detail', 'RealisasiController@detail');
            Route::post('/delete', 'RealisasiController@delete');
            Route::post('/filter','RealisasiController@filterRealisasi');
            Route::post('/add-revisi', 'RealisasiController@formRevisi');
            Route::post('/save-revisi', 'RealisasiController@saveRevisi');
            Route::post('/verifikasi','RealisasiController@verifikasi');
        });
    });
	});