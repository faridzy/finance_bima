<?php

    Route::post('/login-pegawai','Api\Pegawai\ApiLoginPegawaiController@login');
    Route::post('/register-client','Api\Client\ApiRegisterClientController@save');
    Route::post('/login-client','Api\Client\ApiLoginClientController@login');
    Route::get('/list-client/{id}','Api\Pegawai\ApiListClientController@list');
    Route::post('/update-profile/{id}','Api\Client\ApiUpdateProfileController@update');
    Route::get('/data-kategori','Api\Client\ApiInputanController@all');
    Route::post('/add-inputan/{id}','Api\Client\ApiInputanController@add');
    Route::get('/hitungan/{id}','Api\Client\ApiInputanController@hitungan');
    Route::get('/getUserId/{id}','Api\Client\ApiUpdateProfileController@getUserById');
    Route::post('/list-history-client/{id}','Api\Client\ApiHistoryController@list');
    Route::post('/detail-history-client/{id}','Api\Client\ApiHistoryController@detail');
    Route::get('/artikel','Api\Client\ApiArtikelController@index');
    Route::get('/detail-artikel/{id}','Api\Client\ApiArtikelController@detail');








    Route::get('/get-pelanggan/{id}','ApiPelangganController@getPelanggan');
    Route::get('/get-pelanggaran','ApiPelanggaranController@index');
    Route::post('/add-realisasi','ApiRealisasiController@addRealisasi');
    
    Route::get('/view-realisasi/{id}','ApiRealisasiController@viewDetailRealisasi');
    Route::get('/list-revisi-baru/{id}','ApiRealisasiController@listRealisasiRevisiBaru');
    Route::get('/view-realisasi-revisi-baru/{id}','ApiRealisasiController@viewRealisasiRevisiBaru');
    Route::get('/list-revisi-kirim/{id}','ApiRealisasiController@listRealisasiRevisiKirim');
    Route::get('/view-realisasi-revisi-kirim/{id}','ApiRealisasiController@viewRealisasiRevisiKirim');
    Route::post('/update-realisasi/{id}','ApiRealisasiController@updateRealisasiRevisi');
    Route::get('/count-baru/{id}','ApiRealisasiController@countBaru');
    Route::get('/count-revisi/{id}','ApiRealisasiController@countRevisi');
    Route::get('/count-coba/{id}','ApiCoba@countCoba');