-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 08 Jul 2019 pada 01.23
-- Versi server: 10.1.39-MariaDB
-- Versi PHP: 7.1.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `financial`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `artikel`
--

CREATE TABLE `artikel` (
  `id` int(11) NOT NULL,
  `author` varchar(100) NOT NULL,
  `tittle` varchar(100) NOT NULL,
  `text_header` varchar(100) NOT NULL,
  `text` text NOT NULL,
  `images` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `artikel`
--

INSERT INTO `artikel` (`id`, `author`, `tittle`, `text_header`, `text`, `images`, `created_at`, `updated_at`) VALUES
(2, 'https://tatadana.com/apa-itu-financial-check-up/', 'Apa itu Financial Checkup', 'Istilah Health Check Up, atau tes kesehatan maka Financial Check Up, adalah', 'Kalau kita sering mendengar istilah Health Check Up, atau tes kesehatan maka Financial Check Up, adalah tes untuk kondisi keuangan kita.\n\nTerkadang kita merasa baik baik saja dalam keuangan kita, tapi pernahkah kita tau bagaimana kondisi sebenarnya ? nah, langkah awal dalam pengelolaan keuangan yang baik adalah melakukan Financial Check Up, agar kita bisa mengetahui kondisi keuangan kita yang sebenarnya.\n\nKenapa perlu Financial Check Up ?\n\nDengan financial check up kita bisa tahu kondisi keuangan kita yang sebenarnya, masalah apa yang terjadi, dan bisa mengambil langkah yang tepat untuk memperbaikinya.\n\nSeperti kita melakukan tes kesehatan, yang terpenting adalah dokter bisa menentukan obat yang tepat untuk penyembuhan. Maka dalam Financial Check Up, mengambil langkah yang tepat untuk memperbaiki keuangan kita adalah hal yang terpenting.\n\nHal apa saja yang diteliti dalam Financial Check Up ?\n\n1. Kondisi likuiditas\n\nLikuiditas menjadi hal yang penting untuk dimiliki dalam keuangan, karena kondisi darurat keuangan kadang terjadi tanpa kita inginkan. Kalau aset kita semuanya dalam bentuk tidak lancar, seperti rumah, tanah, mobil, tapi kita tidak punya cash. Maka saat kita membutuhkan dana darurat, akan timbul masalah karena diperlukan waktu untuk menjual aset kita.\n\n2. Kondisi Hutang\n\nHutang yang berlebihan dan hutang konsumtif adalah hal yang memberatkan kondisi keuangan kita. Meneliti masalah hutang yang mengganggu keuangan adalah hal wajib bagi setiap orang.\n\n3. Menabung dan Investasi\n\nMenabung dan investasi, apakah sudah dilakukan ? apakah cukup tabungan kita ? apakah investasi kita tepat ?\n\nYuk kita cek kondisi keuangan kita.', '20190620011526_financial.png', '2019-06-20 01:15:26', '2019-06-20 01:15:26'),
(3, 'https://www.cermati.com/artikel/mengapa-mengatur-keuangan-itu-penting-inilah-alasannya', 'Mengapa Mengatur Keuangan itu Penting?', 'Mengatur keuangan itu penting, inilah alasannya ...', 'Uang seringkali menjadi sumber masalah yang bisa menimbulkan masalah besar. Tentu saja ini terjadi karena hampir seluruh kegiatan yang kita lakukan dalam hidup ini membutuhkan uang. Oleh karena itu, uang menjadi hal yang sangat dicari dan orang-orang rela untuk bekerja sekeras mungkin untuk mendapatkannya. Melihat betapa pentingnya uang dalam hidup kita, menyebabkan kita merasa bahwa kita tidak akan bisa hidup tanpa uang. Tidak punya uang menjadi salah satu ketakutan terbesar dalam hidup kita. Pernah kah kamu mengalami kehabisan uang meski kamu sudah bekerja sekeras mungkin dan sudah berhasil mendapatkan uang yang cukup banyak? Sekeras apapun kamu berusaha mengumpulkan uang, kamu tidak akan bisa lepas dari masalah kekurangan uang jika kamu tidak memahami bahwa kamu harus mengatur kruangan. Mengatur keuangan itu penting, tapi mengapa itu penting? Inilah alasannya.\n\n1. Membiasakan Kamu untuk Hidup Teratur\n\nKebiasaan buruk yang paling umum dilakukan orang adalah tidak teratur dalam mengeluarkan uang. Tentu kamu pasti tahu bahwa mengumpulkan uang jauh lebih sulit daripada menghabiskannya. Untuk mendapatkan uang, kamu harus bekerja dan berusaha dulu, sedangkan, uangmu bisa habis seketika tanpe perlu ada usaha atau bahkan pikiran untuk menghabiskannya. Hal ini terjadi karena kamu tidak terbiasa untuk teratur dalam mengatur keuanganmu. Dengan mengatur keuangan, kamu dapat mengatur kemana saja pengeluaran uang kamu sehingga seluruh pengeluaran kamu lebih terencana dan lebih jelas.\n\n2. Membiasakan Kamu untuk Hidup Hemat\n\nHidup hemat tidaklah mudah untuk dilakukan. Banyak orang yang berjuang untuk bisa membiasakan diri hidup hemat. Keinginan kamu untuk berhemat akan terwujud jika kamu pandai mengatur keuangan. Pengertian dari hemat disini bukanlah tidak mengeluarkan uang sama sekali, tetapi kamu menggunakan uang untuk hal-hal yang kamu butuhkan. Membedakan kebutuhan dan keinginan kamu adalah hal yang penting, supaya kamu bisa menentukan mana yang harus kamu lakukan sekarang dan yang mana yang bisa kamu tunda. Dengan pandai mengatur uang, kamu akan terbiasa untuk hidup hemat dan efisien.\n\n3. Membuat Kamu Selalu Tenang\n\nSeringkali, ketika kamu tidak punya uang, segala sesuatu menjadi semakin berat untuk dijalani. Memiliki uang yang cukup membuat hidup kita tenang. Mengapa punya uang bikin hidup tenang? Karena terkadang ada sumber pengeluaran yang tidak terduga kehadirannya dan itu membutuhkan uang. Oleh karena itu, dengan kamu dapat mengatur keuangan kamu, kamu dapat merencanakan uang untuk pengeluaran yang tidak terduga ini sehingga wkamu bisa tetap tenang kapanpun. Masalah terberat sekalipun akan terasa lebih ringan jika kamu sudah memiliki persiapan yang matang untuk menghadapinya.\n\n4. Mengeluarkan Uang Menjadi Lebih Leluasa\n\nTanpa adanya pengaturan keuangan yang baik, terkadang banyak orang jadi takut dalam mengeluarkan uang. Padahal uang itu digunakan untuk kebutuhan yang memang seharusnya. Hal ini membuat kamu menjalani hidup di bawah rasa ketakutan akan kehabisan uang nantinya. Jika kamu mengatur keuangan kamu, sudah ada pembagian pengeluaran yang jelas. Jadi, kamu dapat lebih leluasa dalam mengeluarkan uang untuk kebutuhanmu itu.\n\n5. Melatih Kamu untuk Menabung\n\nApa kamu mau punya uang hanya di awal bulan saja? Tentu tidak bukan? Setiap orang pasti ingin memiliki uang yang bisa ada kapan saja. Menabung adalah jawabannya. Tapi menabung itu tidak seperti membalikkan telapak tangan. Kamu perlu berusaha untuk bisa memiliki tabungan. Cara agar kamu bisa menabung adalah dengan mengatur keuangan. Dengan adanya catatan pengeluara yang jelas setiap bulannya, kamu bisa menyisihkan beberapa uang kamu untuk ditabung.\n\n6. Memungkinkan Kamu untuk Berinvestasi\n\nSebelum kamu melakukan investasi, kamu harus mempunyai uang yang akan kamu pakai untuk diinvestasikan. Tanpa uang modal investasi, apa yang bisa kamu investasikan? Tidak ada. Uang modal ini bisa didapat dari hasil tabunganmu selama ini yang kamu kumpulkan karena kamu mengatur uangmu dengan baik.\n\nPandailah Mengatur Keuanganmu\nItulah alasan mengapa mengatur keuangan adalah hal yang penting untuk dilakukan. Dengan kemampuan kamu dalam mengatur keuangan, banyak hal positif yang akan kamu raih. Keinginan kamu untuk dapat banyak uang akan dapat tercapai melalui hal ini. Jadilah orang yang pandai mengatur keuangan. Semakin pandai kamu mengaturnya, maka semakin besar juga kemungkinan kamu untuk memiliki uang yang banyak di masa depan. Perlu diingat, mengatur keuangan bukan hal yang mudah, jadi sangat wajar jika kamu mengalami kegagalan di awal. Tapi belajarlah dari kegagalan itu dan kamu akan menjadi orang yang pandai dalam mengatur keuangan.', '20190621090307_saving.jpeg', '2019-06-21 09:03:07', '2019-06-21 09:03:07');

-- --------------------------------------------------------

--
-- Struktur dari tabel `client`
--

CREATE TABLE `client` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `pekerjaan` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `tgl_lahir` date NOT NULL,
  `telp` varchar(255) NOT NULL,
  `pegawai_code` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `client`
--

INSERT INTO `client` (`id`, `nama`, `pekerjaan`, `alamat`, `email`, `password`, `tgl_lahir`, `telp`, `pegawai_code`, `created_at`, `updated_at`) VALUES
(2, 'tara budiman', 'wiraswasta', 'Jl. kertanegara no 37, gedangan, sidoarjo', 'tara_bud@gmail.com', '54a8445e42535513337134855be214b58a64c8d5', '1970-10-10', '082222222222', '4', '2019-04-24 04:33:55', '2019-04-25 04:52:36'),
(44, 'Salwa F', 'wiraswasta', 'jl. sawahan no 2, sawahan, surabaya', 'Zauza@gmail.com', '5cbc374b32a1701114146243f2a51dceb3e8efaa', '1970-10-10', '0822222433', '4', '2019-05-12 16:44:55', '2019-06-18 09:56:02'),
(46, 'Bima', 'Wiraswasta', 'Jl. Cakra no 2, karangpilang, surabaya', 'Bima@yahoo.com', '91584746e4f729423f8347a73e845593dc88ccc2', '1990-06-01', '08888888', '4', '2019-06-12 15:29:30', '2019-06-12 15:29:30'),
(47, 'farid', 'wiraswasta', '', 'farid@yahoo.com', '6a214fde6c1f8c84902a5576bbe98834623913cc', '1970-10-10', '0888888888', '5', '2019-06-12 19:39:55', '2019-06-12 19:39:55'),
(48, 'Aldo', 'Wiraswasta', 'Jl. Jagir no 32, Surabaya', 'Aldo@gmail.com', '1c89c0f71ac97754ffc597c567d01b2ade0c9324', '1982-06-02', '08223456789', '6', '2019-07-07 14:20:38', '2019-07-07 14:20:38');

-- --------------------------------------------------------

--
-- Struktur dari tabel `history`
--

CREATE TABLE `history` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `ratio_id` int(11) NOT NULL,
  `pegawai_id` int(11) NOT NULL,
  `value` double NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `history_statement`
--

CREATE TABLE `history_statement` (
  `id` int(11) NOT NULL,
  `history_id` int(11) NOT NULL,
  `rule_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE `kategori` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`id`, `nama`, `created_at`, `updated_at`) VALUES
(1, 'Asset', '2019-03-20 06:56:26', '2019-03-20 06:56:26'),
(2, 'Hutang', '2019-03-20 11:10:48', '2019-03-20 11:10:48'),
(3, 'Pengeluaran', '2019-03-20 11:10:57', '2019-03-20 11:10:57'),
(4, 'Pemasukan', '2019-03-20 11:11:06', '2019-03-20 11:11:06');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pegawai`
--

CREATE TABLE `pegawai` (
  `id` int(11) NOT NULL,
  `code_agent` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `user_type` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pegawai`
--

INSERT INTO `pegawai` (`id`, `code_agent`, `nama`, `email`, `password`, `tgl_lahir`, `user_type`, `created_at`, `updated_at`) VALUES
(1, '00000001', 'admin', 'admin@admin.com', 'd033e22ae348aeb5660fc2140aec35850c4da997', '2017-07-02', 1, '2019-03-13 00:00:00', '2019-03-13 00:00:00'),
(4, '00000002', 'dwi', 'dwi@admin.com', '3da5fc0bfdbf2b0e1ba2b171771be854749d9453', '2019-03-20', 2, '2019-03-20 06:26:38', '2019-03-20 06:26:38'),
(5, '00000003', 'Zaenal', 'zen@gmail.com', '3fb05a163b3b0a5ac0f6fde949419b62362ccf00', '2019-06-12', 2, '2019-06-12 19:39:10', '2019-06-12 19:39:10'),
(6, '02226099', 'Elita Ovina', 'elitaov@gmail.com', '793aebd3ae4ff9ebfd57b08c3d8366cd2a4333ef', '1987-05-14', 2, '2019-07-07 10:48:16', '2019-07-07 10:48:16');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ratio`
--

CREATE TABLE `ratio` (
  `id` int(11) NOT NULL,
  `nama_ratio` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ratio`
--

INSERT INTO `ratio` (`id`, `nama_ratio`, `created_at`, `updated_at`) VALUES
(1, 'Likuiditas', '2019-03-21 11:49:10', '2019-03-21 11:55:47'),
(2, 'Aset Likuiditas dibanding Nilai Bersih Kekayaan', '2019-03-21 11:53:28', '2019-03-21 11:53:28'),
(3, 'Tabungan', '2019-03-21 11:53:49', '2019-03-21 11:53:49'),
(4, 'Perbandngan Hutang terhadap Aset', '2019-03-21 11:54:06', '2019-03-21 11:54:06'),
(5, 'Kemampuan Pelunasan Hutang', '2019-03-21 11:54:27', '2019-03-21 11:54:27'),
(6, 'Perbandingan Nilai Bersih Aset Investasi terhadap Nilai Bersih Kekayaan', '2019-03-21 11:55:23', '2019-04-25 08:46:01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rule`
--

CREATE TABLE `rule` (
  `id` int(11) NOT NULL,
  `range` varchar(255) NOT NULL,
  `nilai` varchar(255) NOT NULL,
  `statement` text NOT NULL,
  `saran` text NOT NULL,
  `ratio_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `rule`
--

INSERT INTO `rule` (`id`, `range`, `nilai`, `statement`, `saran`, `ratio_id`, `created_at`, `updated_at`) VALUES
(1, '0-3', 'Tidak Ideal', 'Hasil dari rasio likuiditas keuangan anda adalah dibawah 3 bulan yang berarti tidak ideal. Sedangkan rasio ideal dari rasio likuiditas 3-6 bulan yang digunakan sebagai cadangan uang untuk menyokong bila terjadinya pengeluaran mendadak', 'Maka dari itu saran dari saya perbesar sumber income anda atau meminimalisir sumber pengeluaran yang tidak perlu.', 1, '2019-03-21 12:07:56', '2019-04-25 08:25:17'),
(3, '3-6', 'Ideal', 'Hasil dari rasio likuiditas keuangan anda adalah ideal, dikarenakan hasil rasio anda diantara 3-6 bulan, yang digunakan sebagai cadangan uang untuk menyokong bila terjadinya pengeluaran mendadak', 'Maka dari itu saran dari saya pertahankan manajemen keuangan anda saat ini atau kalau bisa tingkatkan sumber penghasilan anda', 1, '2019-04-25 08:26:57', '2019-04-25 08:26:57'),
(4, '6-24', 'Sangat Bagus', 'Selamat rasio likuiditas anda lebih dari jumlah ideal dari asset likuiditas yang digunakan sebagai cadangan uang untuk menyokong bila terjadinya pengeluaran mendadak', 'Maka dari itu saran dari saya pertahankan manajemen keuangan anda saat ini dan cobalah untuk mendiversifikasikan portofolio keuangan anda', 1, '2019-04-25 08:28:05', '2019-04-25 08:28:05'),
(5, '0-10', 'Kurang Ideal', 'Hasil dari rasio tabungan anda dibawah 10% yang berarti kurang ideal. Sedangkan rasio idealnya adalah minimal 10%. Dimana rasio ini adalah sebuah indicator yang menyatakan pendapatan yang disisihkan untuk penggunaan di masa depan (dalam bentuk simpanan/tabungan)', 'Maka dari itu saran dari saya, yaitu untuk meningkatkan pos tabungan anda dalam bentuk cash di dalam tabungan ATM atau deposit setiap bulannya', 3, '2019-04-25 08:32:49', '2019-04-25 08:32:49'),
(6, '11-100', 'Ideal', 'Selamat, hasil dari rasio tabungan anda menyatakan bahwa tabungan anda cukup ideal, melebihi 10% dari pendapatan kotor anda. Dimana rasio ini adalah sebuah indicator yang menyatakan pendapatan yang disisihkan untuk penggunaan di masa depan (dalam bentuk simpanan/tabungan)', 'Maka dari itu saran dari saya, pertahankan pos pengeluaran anda untuk menabung dalam bentuk cash di ATM maupun deposit setiap bulannya. Bila perlu tingkatkan porsi menabung anda setiap tahunnya seiring bertambahnya usia, untuk keperluan di hari tua (pensiun)', 3, '2019-04-25 08:34:39', '2019-04-25 08:34:39'),
(7, '0-50', 'Aman', 'Selamat, hasil dari rasio Perbandingan Hutang terhadap banyaknya asset anda adalah ideal, yang berarti posisi ini anda cukup aman. Karena pada rasio ini adalah indikator kemampuan seorang individu dalam membayar hutang-hutangnya', 'Maka dari itu saran dari saya, pertahankan rasio ini setiap tahunnya. Namun dengan seiringnya berjalan waktu, rasio ini harus menjadi 0% saat mendekati pensiun, agar di masa yang akan dating, anda dapat menikmati hasil dari asset anda tanpa perlu memikirkan pengeluaran untuk hutang', 4, '2019-04-25 08:35:35', '2019-04-25 08:35:35'),
(8, '51-100', 'Tidak Aman', 'Hasil dari rasio hutang terhadap total asset anda kurang ideal atau kurang aman, dikarenakan hasil rasio hutang anda adalah diatas 50% dari total asset yang anda miliki. Rasio ideal pada rasio ini adalah dibawah 50%, Karena pada rasio ini adalah indikator kemampuan seorang individu dalam membayar hutang-hutangnya', 'Maka dari itu saran dari saya, untuk mengurangi hutang jangka pendek maupun jangka Panjang anda yang sekiranya hutang itu dikeluarkan untuk pengeluaran yang konsumtif (seperti kredit mobil, Kartu Kredit).atau anda bisa meningkatkan total Asset anda, yang dimana asset ini adalah asset berjalan yang bisa menjadi passive income anda. Diusahakan dan harus diusahakan seiring dengan berjalannya waktu porsi hutang anda semakin menurun sehingga di masa pension anda, rasio hutang anda terhadap asset menjadi 0%', 4, '2019-04-25 08:36:42', '2019-04-25 08:36:42'),
(9, '0-15', 'Kurang Ideal', 'Hasil dari rasio Aset likuid (asset yang mudah dicairkan dalam bentuk cash) anda adalah kurang ideal, karena rasio yang ideal adalah diatas 15%. Rasio ini mengindikasikan seberapa banyak jumlah nilai bersih kekayaan seseorang dalam bentuk kas atau  setara kas (tunai). Rasio ini bertujuan untuk memelihara likuiditas dari asset likuid anda yang digunakan untuk emergency fund (dana darurat).', 'Maka dari itu saya sarankan anda untuk meningkatkan porsi asset anda dalam bentuk kas atau setara kas (tabungan ATM ataupun deposito), yang bertujuan untuk memelihara kelikuiditas asset anda yang akan digunakan untuk kebutuhan atau keperluan yang mendadak (emergency fund) seperti untuk kebutuhan sakit mendadak, service motor/mobil yang rusak mendadak, atau kebutuhan keluarga yang mendadak sekalipun', 2, '2019-04-25 08:38:42', '2019-04-25 08:38:42'),
(10, '16-100', 'Ideal', 'Selamat hasil dari rasio anda menyatakan bahwa asset likuid (asset yang mudah dicairkan dalam bentuk cash) cukup ideal. Karena rasio anda diatas 15% dibanding nilai bersih kekayaan anda (total asset). Rasio ini mengindikasikan seberapa banyak jumlah nilai bersih kekayaan seseorang dalam bentuk kas atau setara kas (tunai). Rasio ini bertujuan untuk memelihara likuiditas dari asset likuid anda yang digunakan untuk emergency fund (dana darurat)', 'Maka dari itu saran dari saya adalah pertahankan angka rasio ini. Rasio ini didapatkan dari asset anda dalam bentuk kas atau setara kas (tabungan ATM ataupun deposito), yang bertujuan untuk memelihara kelikuiditas asset anda yang akan digunakan untuk kebutuhan atau keperluan yang mendadak (emergency fund) seperti untuk kebutuhan sakit mendadak, service motor/mobil yang rusak mendadak, atau kebutuhan keluarga yang mendadak sekalipun', 2, '2019-04-25 08:39:50', '2019-04-25 08:39:50'),
(11, '0-35', 'Ideal', 'Selamat hasil dari rasio anda menyatakan bahwa kemampuan dalam pelunasan hutang tahunan anda, dimana rasio kurang dari 35% adalah rasio ideal. Rasio ini menunjukan berapa banyak jumlah pendapatan yang dibutuhkan dalam setahun untuk membayar total hutang tahunan', 'Maka dari itu saya menyarankan agar anda memperthankan angka rasio ini. Namun seiring berjalannya waktu disaat usia anda semakin senior untuk tidak menambah pinjaman/hutang anda. Jika ingin menambah pun anda harus menambah hutang dalam bentuk pembelian asset yang bisa menjadi passive income anda (rumah untuk disewakan/ kos) yang bisa menjadi tambahan sumber pendapatan tahunan anda', 5, '2019-04-25 08:41:18', '2019-04-25 08:41:18'),
(12, '36-100', 'Tidak Ideal', 'Hasil dari rasio anda menyatakan bahwa kemampuan dalam pelunasan hutang tahunan anda kurang ideal, dimana rasio lebih dari 35% adalah rasio ideal. Rasio ini menunjukan berapa banyak jumlah pendapatan yang dibutuhkan dalam setahun untuk membayar total hutang tahunan anda', 'Maka dari itu saya menyarankan agar anda untuk mengurangi angka rasio ini, dengan cara . untuk tidak menambah pinjaman/hutang (kartu kredit, kredit mobil/motor) anda menjadi rasio maksimal 35% , dan menambah sumber pendapatan tahunan anda', 5, '2019-04-25 08:42:19', '2019-04-25 08:42:19'),
(13, '0-50', 'Kurang Ideal', 'Hasil dari rasio anda menyatakan bahwa nilai bersih asset investasi anda sangat sedikit dibanding dengan nilai bersih kekayaan anda. Dimana rasio yang ideal adalah minimal 50%. Rasio bertujuan untuk membantu untuk menunjukan seberapa baik seorang individu dalam melipatgandakan total kapitalnya/assetnya.', 'Maka dari itu disarankan untuk menambah asset investasi anda, jika anda ingin melipat gandakan nilai kekayaan anda', 6, '2019-04-25 08:44:35', '2019-04-25 08:44:35'),
(14, '51-100', 'Ideal', 'Selamat, Hasil dari rasio anda menyatakan bahwa nilai bersih asset investasi anda ideal Dimana rasio yang ideal adalah minimal 50%. Rasio bertujuan untuk membantu untuk menunjukan seberapa baik seorang individu dalam melipatgandakan total kapitalnya/assetnya', 'Maka dari itu disarankan untuk mempertahan rasio persentase total asset investasi anda. Persentase ini harus semakin membesar seiring semakin dekatnya waktu pensiun, dikarenakan asset investasi ini bisa menjadi passive income anda disaat anda pensiun nanti', 6, '2019-04-25 08:45:29', '2019-04-25 08:45:29');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sub_kategori`
--

CREATE TABLE `sub_kategori` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `kategori_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sub_kategori`
--

INSERT INTO `sub_kategori` (`id`, `nama`, `parent_id`, `kategori_id`, `created_at`, `updated_at`) VALUES
(3, 'Kas', 0, 1, '2019-04-25 06:15:25', '2019-04-25 06:15:25'),
(4, 'Pribadi', 0, 1, '2019-04-25 06:26:15', '2019-04-25 06:26:15'),
(5, 'Investasi', 0, 1, '2019-04-25 06:26:51', '2019-04-25 06:26:51'),
(6, 'Jangka Pendek', 0, 2, '2019-04-25 06:27:34', '2019-04-25 06:27:34'),
(7, 'Jangka Panjang', 0, 2, '2019-04-25 06:27:47', '2019-04-25 06:27:56'),
(8, 'Gaji Tetap', 0, 4, '2019-04-25 06:52:29', '2019-04-25 06:52:29'),
(9, 'Gaji tidak Tetap', 0, 4, '2019-04-25 06:53:01', '2019-04-25 06:53:01'),
(10, 'Tabungan', 0, 3, '2019-04-25 06:54:15', '2019-04-25 06:54:15'),
(11, 'Sosial', 0, 3, '2019-04-25 06:54:24', '2019-04-25 08:12:46'),
(12, 'Pengeluaran Tetap', 0, 3, '2019-04-25 06:54:45', '2019-04-25 06:54:45'),
(17, 'Saving', 3, 1, '2019-04-25 08:01:45', '2019-04-25 08:01:54'),
(18, 'Deposito', 3, 1, '2019-04-25 08:02:05', '2019-04-25 08:02:05'),
(19, 'Rumah', 4, 1, '2019-04-25 08:02:28', '2019-04-25 08:02:28'),
(20, 'Mobil', 4, 1, '2019-04-25 08:02:41', '2019-04-25 08:02:41'),
(21, 'Motor', 4, 1, '2019-04-25 08:03:06', '2019-04-25 08:03:54'),
(22, 'Barang Pribadi', 4, 1, '2019-04-25 08:03:36', '2019-04-25 08:03:36'),
(23, 'Rumah Sewa', 5, 1, '2019-04-25 08:06:14', '2019-04-25 08:06:14'),
(24, 'Kos', 5, 1, '2019-04-25 08:06:41', '2019-04-25 08:06:41'),
(25, 'UnitLink', 5, 1, '2019-04-25 08:06:55', '2019-04-25 08:07:06'),
(26, 'Portfolio Saham', 5, 1, '2019-04-25 08:07:46', '2019-04-25 08:07:46'),
(27, 'Emas', 5, 1, '2019-04-25 08:08:10', '2019-04-25 08:08:10'),
(28, 'Kemitraan Usaha', 5, 1, '2019-04-25 08:08:29', '2019-04-25 08:08:29'),
(29, 'Kartu Kredit', 6, 2, '2019-04-25 08:08:54', '2019-04-25 08:08:54'),
(30, 'Kredit Kendaraan', 7, 2, '2019-04-25 08:09:27', '2019-04-25 08:09:27'),
(31, 'Pinjaman Hipotek', 7, 2, '2019-04-25 08:10:05', '2019-04-25 08:10:05'),
(32, 'Pengeluaran Variabel', 0, 3, '2019-04-25 08:15:17', '2019-04-25 08:15:17'),
(33, 'KPR', 12, 3, '2019-04-25 08:15:48', '2019-04-25 08:15:48'),
(34, 'Tambahan KPR', 12, 3, '2019-04-25 08:16:07', '2019-04-25 08:16:07'),
(35, 'Kredit kendaraan tetap', 12, 3, '2019-04-25 08:16:56', '2019-04-25 08:16:56'),
(36, 'Asuransi', 12, 3, '2019-04-25 08:17:23', '2019-04-25 08:17:23'),
(37, 'Pajak', 32, 3, '2019-04-25 08:18:16', '2019-04-25 08:18:16'),
(38, 'Lifestyle', 32, 3, '2019-04-25 08:18:51', '2019-04-25 08:18:51'),
(39, 'Liburan', 32, 3, '2019-04-25 08:19:23', '2019-04-25 08:19:23'),
(40, 'Kebutuhan Pokok', 32, 3, '2019-04-25 08:19:48', '2019-04-25 08:19:48');

-- --------------------------------------------------------

--
-- Struktur dari tabel `value`
--

CREATE TABLE `value` (
  `id` int(11) NOT NULL,
  `value` double NOT NULL DEFAULT '0',
  `sub_kategori_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `artikel`
--
ALTER TABLE `artikel`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `history_statement`
--
ALTER TABLE `history_statement`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `ratio`
--
ALTER TABLE `ratio`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `rule`
--
ALTER TABLE `rule`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `sub_kategori`
--
ALTER TABLE `sub_kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `value`
--
ALTER TABLE `value`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `artikel`
--
ALTER TABLE `artikel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `client`
--
ALTER TABLE `client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT untuk tabel `history`
--
ALTER TABLE `history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=291;

--
-- AUTO_INCREMENT untuk tabel `history_statement`
--
ALTER TABLE `history_statement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=276;

--
-- AUTO_INCREMENT untuk tabel `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `ratio`
--
ALTER TABLE `ratio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `rule`
--
ALTER TABLE `rule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `sub_kategori`
--
ALTER TABLE `sub_kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT untuk tabel `value`
--
ALTER TABLE `value`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1216;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
